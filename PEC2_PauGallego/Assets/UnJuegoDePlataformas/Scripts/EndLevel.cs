using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public GameObject menu;
    public AudioSource endSound;

    void Start()
    {
        endSound = GetComponent<AudioSource>();
    }
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            menu.SetActive(true);
            endSound.Play();
        }
    }
}
