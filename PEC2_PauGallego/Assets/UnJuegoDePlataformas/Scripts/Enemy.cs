using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public MainCharacter Mario;
    private Rigidbody2D rb;
    
    public float speed;
    public bool direction;

    void Update()
    {
        if (direction)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            if (!Mario.isGrounded())
            {
                Destroy(gameObject);
            }
        }
        if (collision.CompareTag("Limit")) {direction = !direction;}
    }
}
