using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacter : MonoBehaviour
{
    // Definición de objetos
    private Rigidbody2D rb;
    public GameplayManager GameplayManager;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public GameObject marioUpgraded;
    private SpriteRenderer mario;

    public AudioSource marioSound;
    public AudioClip jumpSound;
    public AudioClip steepSound;
    public AudioClip upgradeSound;
    public AudioClip damageSound;
    public AudioClip deadSound;

    // Definición de variables
    public float acceleration;
    public float jumpPower;
    public float fallMultiplier;
    public float invulnerableDuration;

    private float horizontal;
    private float velocity;
    private Vector2 vecGravity;
    public byte hp;
    private bool invulnerable;
    public bool grounded;
    private bool movement;

    void Start()
    {
        mario = GetComponent<SpriteRenderer>();

        rb = GetComponent<Rigidbody2D>();
        
        vecGravity = new Vector2(0, -Physics2D.gravity.y);
        
        marioStatChecker();
        invulnerable = false;
        movement = true;
        
        marioSound = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        // Movimiento horizontal del personaje principal
        horizontal = Input.GetAxisRaw("Horizontal");

        if (movement)
        {
            if (horizontal != 0.0f) {velocity = Mathf.Clamp(velocity + horizontal * acceleration * Time.deltaTime, -1.0f, 1.0f);}
            else {velocity -= velocity * acceleration * Time.deltaTime;}
        
            transform.Translate(Vector3.right * velocity * acceleration * Time.deltaTime);
        }

        // Dirección del personaje
        if (horizontal > 0) {flipX(false);}
        if (horizontal < 0) {flipX(true);}

        // Salto del personaje principal
        grounded = isGrounded();
        
        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            marioSound.clip = jumpSound;
            marioSound.Play();
        }
        if (rb.velocity.y < 0) {rb.velocity -= vecGravity * fallMultiplier * Time.deltaTime;}
    }
    public bool isGrounded()
    {
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.8f, 0.15f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }
    public void flipX(bool flip)
    {
        Vector3 scale = transform.localScale;
        scale.x = flip ? Mathf.Abs(scale.x) * -1f : Mathf.Abs(scale.x);
        transform.localScale = scale;
    }

    public void marioStatChecker()
    {
        switch (hp)
        {
            case 0:
                dead();
                break;
            case 1:
                noUpgrade();
                break;
            case 2:
                marioUpgrade();
                break;
        }
    }
    public void noUpgrade()
    {
        marioUpgraded.SetActive(false);
        mario.enabled = true;
    }
    public void marioUpgrade()
    {
        marioUpgraded.SetActive(true);
        mario.enabled = false;
        marioSound.clip = upgradeSound;
        marioSound.Play();;
    }
    public void dead()
    {
        StartCoroutine(TimerToSpawn());
    }
    IEnumerator TimerToSpawn()
    {
        marioSound.clip = deadSound;
        marioSound.Play();
        mario.enabled = false;
        movement = false;
        yield return new WaitForSeconds(3f);
        GameplayManager.RestartLevel();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (invulnerable == false)
            {
                if (isGrounded())
                {
                    hp--;
                    marioStatChecker();
                    if (hp > 0)
                    {
                        marioSound.clip = damageSound;
                        marioSound.Play();
                        StartCoroutine(invulnerableOn());
                    }
                }
                else
                {
                    marioSound.clip = steepSound;
                    marioSound.Play();
                }
            }
        }
        IEnumerator invulnerableOn()
        {
            invulnerable = true;
            yield return new WaitForSeconds(invulnerableDuration);
            invulnerable = false;
        }

        if (collision.CompareTag("DeadZone"))
        {
            hp = 0;
            marioStatChecker();
        }
        
        if (collision.CompareTag("Upgrade"))
        {
            hp = 2;
            marioStatChecker();
        }

        if (collision.CompareTag("End"))
        {
            movement = false;
        }
    }
}
