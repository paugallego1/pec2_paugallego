# Documento del PEC2: Un juego de plataformas de Pau Gallego

[JUGAR JUEGO AQUÍ](https://ghalek.itch.io/pec2-unjuegodeplataformas)

## Descripción del juego

El juego es una recreación del nivel 1.1 de Super Mario Bross. Al igual que en el juego original; el jugador controla a Mario el cual solo puede desplazarse horizontalmente y saltar y para completar los niveles deberá alcanzar al final de los mismos. Hay obstáculos y enemigos que matan de manera instantánea a Mario pero Mario puede obtener una mejora la cúal le permite ser más grande y resistir un golpe más.

# Desarrollo del juego

## Creación del personaje principal (Mario)

Para programar al Mario, primero se ha realizado el movimiento básico horizontal. Utilizando los inputs asociado al movimiento horizontal que trae por defecto Unity (A, D y las flechas horizontales del teclado) el jugador puede desplazarse hacia la derecha o la izquierda. Dentro de la función Update se ha asociado una variable a dicho input para determinar hacia que dirección, del eje de la x, se desplaza el personaje. Este valor se multiplica con otra variable que determina qué tan rápido es Mario.

```
    horizontal = Input.GetAxisRaw("Horizontal");

    if (horizontal != 0.0f) {velocity = Mathf.Clamp(velocity + horizontal * acceleration * Time.deltaTime, -1.0f, 1.0f);}
     else {velocity -= velocity * acceleration * Time.deltaTime;}
        
    transform.Translate(Vector3.right * velocity * acceleration * Time.deltaTime);
```

También se ha tenido en cuenta que el sprite del personaje mire en la dirección en la que corre. Para ello se hace una función que escala por el mismo valor pero en negativo dependiendo de si el valor de horizontal es negativo o positivo.

```
    if (horizontal > 0) {flipX(false);}
    if (horizontal < 0) {flipX(true);}

    public void flipX(bool flip)
    {
        Vector3 scale = transform.localScale;
        scale.x = flip ? Mathf.Abs(scale.x) * -1f : Mathf.Abs(scale.x);
        transform.localScale = scale;
    }
```

Para el salto se aplica una fuerza hacia arriba al utilizar el input de salto (Barra espaciadora) y con la condición de que Mario toque el suelo. La condición se ha creado para evitar que Mario pueda saltar de manera indefinida y volar. Se ha creado una cápsula en los pies del personaje que comprueba si toca el suelo o no. Eso también se hace para evitar que Mario pueda escalar superficies horizontales ya que si se usara el propio collider del personaje, el jugador podría saltar de manera indefinida si está continuamente tocando una pared aunque no toque el suelo.

```
    if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
        }

    if (rb.velocity.y < 0) {rb.velocity -= vecGravity * fallMultiplier * Time.deltaTime;}

    bool isGrounded()
    {
        return Physics2D.OverlapCapsule(groundCheck.position, new Vector2(0.8f, 0.15f), CapsuleDirection2D.Horizontal, 0, groundLayer);
    }
```

También se han creado los 3 estados posibles del personaje; normal o pequeño, muerto y mejorado o grande. El pequeño o normal es el estado inicial de Mario. En este estado, Mario es vulnerable y basta de un solo ataque enemigo para morir. Grande o mejorado es el estado en el que Mario es un poco más poderoso. Cambia su sprite por una versión más grande y detallada, puede resistir un golpe de los enemigos aunque al hacerlo vuelve a su estado pequeño y además puede romper los bloques de ladrillo si los golpea con la cabeza al saltar.

Para representar al Mario grande, se ha creado un gameobject hija del Mario normal el cúal contiene el sprite de Mario grande y un collider más grande. Cuando el MArio grande se activa, se activa también el gameobject del grande pero se desactiva el sprite del pequeño. Como la colisión del pequeño queda dentro de la colisión del grande, no es necesario sustituirla. Para obtener esta mejora, hay que chocar con la seta del principio.

Para el estado de muerte simplemente se reinicia el nivel.

```
    public void noUpgrade()
    {
        marioUpgraded.SetActive(false);
        mario.enabled = true;
    }
    public void marioUpgrade()
    {
        marioUpgraded.SetActive(true);
        mario.enabled = false;
    }
    public void dead()
    {
        GameplayManager.RestartLevel();
    }
```

Para controlar los estados, se ha creado un switch para determinar en qué estado debe estar Mario dependiendo de sus "hit points" o hp. Este switch está dentro de una función que el programa llama cada vez que Mario colisiona con un enemigo o la seta y así evitar tenerla en el Update consumiendo memoria. También se ha creado un estado de "invulnerabilidad" de Mario para que el jugador pueda eliminar a los enemigos sin recibir daño. Esta estado se activa cuando el jugador está en el aire ya que para derrotar a los enemigos hay que caer encima de ellos. Tanto a la seta como al enemigo se le aplica un Destroy al colisionar.

Para reconocer si la colisión es de un enemigo o la seta, se ha utilizado las etiquetas.

```
    public void marioStatChecker()
    {
        switch (hp)
        {
            case 0:
                dead();
                break;
            case 1:
                noUpgrade();
                break;
            case 2:
                marioUpgrade();
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (invulnerable == false)
            {
                if (isGrounded())
                {
                    hp--;
                    marioStatChecker();
                    StartCoroutine(invulnerableOn());
                }
            }
        }      
        if (collision.CompareTag("Upgrade"))
        {
            hp = 2;
            marioStatChecker();
        }
    }
```

## Seguimiento de la cámara

Para crear el seguimiento de la cámara con el personaje se ha programado siguiendo una lógica consistente en avanzar al mismo nivel que personaje principal, siguiendo su misma posición. Cuando el personaje gira hacia atrás, osea la derecha, la cámara se queda fija y no retrocede como ocurre en el juego original. También hay una barrera en el borde derecho de la pantalla para evitar que el personaje escape de la cámara cuando vaya hacia la derecha.

```
    newCamLimit = camera.position.x;

    if (character.position.x > newCamLimit && character.position.x < 196.75)
        {
            Vector3 newPosition = transform.position;
            newPosition.x = character.position.x;
            transform.position = newPosition;
        }
```
Los límites están delimitados físicamente con colisiones dentro del nivel.

## Creación del enemigo (Gommpa)

El enemigo se ha programado de manera que esté siempre moviéndose horizontalmente en un estado constante de patrulla. Con unos colliders invisibles etiquetados como "Limit" hacen que en caso de que el Goompa colisione con uno, cambie de dirección.

```
    void Update()
    {
        if (direction)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainCharacter"))
        {
            Destroy(gameObject);
        }
        if (collision.CompareTag("Limit")) {direction = !direction;}
    }
```

## Sprites

Para implementar los sprites del escenario se ha utilizado una plantilla descargada de una web de recursos de videojuegos clásicos. Para los sprites de Mario, el Goompa y la seta se han recreado con Aseprite, un software de creación de sprites e imágenes con pixel art con una interfaz y uso similar a Adobe Photoshop.

## Cosas a hacer

Hay muchos elementos que al final no se han implementado por falta de tiempo. Estos son; una interfaz funcional con contador de tiempo y puntos, que Mario grande pueda romper bloques, implementación de los bloques sorpresa, el Mario blanco capaz de lanzar proyectiles e implementación de animaciones como correr y saltar.

## Recursos utilizados

- [ ] Sprites de Mario, Goompa y seta: Recreación propia.
- [ ] Sprites de elementos del nivel: [Super Mario Bros](https://www.spriters-resource.com/nes/supermariobros/) de [Spriters resource](https://www.spriters-resource.com).
- [ ] Efectos de sonido: [Super Mario Bros (NES)](https://themushroomkingdom.net/media/smb/wav) de [The Mushroom Kingdom](https://themushroomkingdom.net).
- [ ] Musica de fondo: [Super Mario Bros (NES) Music - Overworld Theme](https://www.youtube.com/watch?v=iy3qq7zc4EY) de [GBelair](https://www.youtube.com/@GBelair).
- [ ] Fuente de texto: [Pixeloid Font Family](https://www.1001fonts.com/pixeloid-font.html) de [GGBotNet](https://www.1001fonts.com/users/GGBotNet/).